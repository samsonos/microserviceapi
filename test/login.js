//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let User = require('../app/models/user');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Security login', () => {

  before((done) => {
    User.remove({}, (err) => {
      done();
    });
  });

  describe('Login with not existing user', () => {
    it('Should get error not found', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'mol91mol@ukr.net', password: 'qwerty'})
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.have.property('error').eql('User not found');
          done();
        });
    });
  });

  describe('Create new user and try to login', () => {
    it('Create new user', (done) => {
      let user = {
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori',
        password: 'qwerty'
      };
      chai.request(server)
        .post('/user')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('Should return user object', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'mol91mol@ukr.net', password: 'qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.user.should.have.property('email');
          res.body.user.should.have.property('firstName');
          res.body.user.should.have.property('lastName');
          res.body.user.should.have.property('password');
          done();
        });
    });
  });
});
