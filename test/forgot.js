//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let User = require('../app/models/user');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Security forgot password', () => {

  before((done) => {
    User.remove({}, (err) => {
      done();
    });
  });

  describe('Forgot password with not existing user', () => {
    it('It should get an error about user not found', (done) => {
      chai.request(server)
        .post('/forgot')
        .send({email: 'mol91mol@ukr.net'})
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.have.property('error').eql('User not found');
          done();
        });
    });
  });

  describe('Create new user and do forgot password', () => {

    it('Should create new user', (done) => {
      let user = {
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori',
        password: 'qwerty'
      };
      chai.request(server)
        .post('/user')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('Should sent email with reminder about password', (done) => {
      chai.request(server)
        .post('/forgot')
        .send({email: 'mol91mol@ukr.net'})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('status').eql(1);
          res.body.should.have.property('message').eql('Email was sent');
          done();
        });
    });
  });
});
