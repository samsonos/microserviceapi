//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
mongoose.Promise = global.Promise;

let User = require('../app/models/user');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let sinon = require('sinon');
let should = chai.should();
chai.use(chaiHttp);

describe('User model', () => {
  beforeEach((done) => {
    User.remove({}, (err) => {
      done();
    });
  });

  describe('/GET users with mocked mongoose', () => {
    it('It should GET all the users', (done) => {
      let query = {exec: () => {}};
      sinon.stub(query, 'exec', function(fn) {
        setTimeout(() => {
          fn([{
            email: 'mol91mol@ukr.net',
            firstName: 'Ruslan',
            lastName: 'Satori',
            password: 'qwerty'
          }]);
        }, 0);
      });

      sinon.stub(User, 'find', function() {
        return query;
      });

      User.find({}).exec((userList) => {
        userList.should.be.a('array');
        userList.length.should.to.be.equal(1);
        done();
      });
      User.find.restore();
    });
  });

  describe('/GET users', () => {
    it('It should GET all the users', (done) => {
      chai.request(server)
        .get('/user')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.to.be.equal(0);
          done();
        });
    });
  });

  describe('/POST user', () => {
    it('it should not POST a user without pages field', (done) => {
      let user = {
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori'
      };
      chai.request(server)
        .post('/user')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('password');
          res.body.errors.password.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should POST a user', (done) => {
      let user = {
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori',
        password: 'qwerty'
      };
      chai.request(server)
        .post('/user')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('User successfully added!');
          res.body.user.should.have.property('email');
          res.body.user.should.have.property('firstName');
          res.body.user.should.have.property('lastName');
          res.body.user.should.have.property('password');
          done();
        });
    });
  });

  describe('/GET/:email user', () => {
    it('It should GET a user by the given email', (done) => {
      let user = new User({
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori',
        password: 'qwerty'
      });
      user.save((err, user) => {
        chai.request(server)
          .get('/user/' + user.email)
          .send(user)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('email');
            res.body.should.have.property('firstName');
            res.body.should.have.property('lastName');
            res.body.should.have.property('password');
            res.body.should.have.property('_id').eql(user.id);
            done();
          });
      });

    });
  });

  describe('/PUT/:email user', () => {
    it('It should UPDATE a user given the email', (done) => {
      let user = new User({
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori',
        password: 'qwerty'
      });
      user.save((err, user) => {
        User.findOne({email: 'mol91mol@ukr.net'}, (err, user) => {
          chai.request(server)
            .put('/user/' + user.email)
            .send({firstName: 'Roman'})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('message').eql('User updated!');
              res.body.user.should.have.property('firstName').eql('Roman');
              done();
            });
        });
      })
    });
  });

  describe('/DELETE/:email user', () => {
    it('It should DELETE a user given the email', (done) => {
      let user = new User({
        email: 'mol91mol@ukr.net',
        firstName: 'Ruslan',
        lastName: 'Satori',
        password: 'qwerty'
      });
      user.save((err, user) => {
        chai.request(server)
          .delete('/user/' + user.email)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('User successfully deleted!');
            res.body.result.should.have.property('ok').eql(1);
            res.body.result.should.have.property('n').eql(1);
            done();
          });
      });
    });
  });
});
