let express = require('express');
let app = express();
let mongoose = require('mongoose');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let port = 5673;
let book = require('./app/routes/user');
let security = require('./app/routes/security');
let config = require('config');

//db options
let options = { 
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } }
};

//db connection      
mongoose.connect(config.DBHost, options);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
app.set('superSecret', config.get('secret'));

//don't show the log when it is test
if(config.util.getEnv('NODE_ENV') !== 'test') {
	//use morgan to log at command line
	app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}

//parse application/json and look for raw text                                        
app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json' }));

app.get('/', (req, res) => res.json({message: 'Hello'}));

app.route('/user')
	.get(book.getUsers)
	.post(book.postUser);
app.route('/user/:email')
	.get(book.getUser)
	.delete(book.deleteUser)
	.put(book.updateUser);

app.route('/login')
	.post(security.login);

app.route('/forgot')
  .post(security.forgot);

app.listen(port);
console.log('Listening on port ' + port);

module.exports = app;