let mongoose = require('mongoose');
let uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    firstName: { type: String, required: true, minlength: 3, maxlength: 30 },
    lastName: { type: String, required: true, minlength: 3, maxlength: 30 },
    email: { type: String, required: true, unique: true, index: {unique: true}},
    password: { type: String, required: true, minlength: 6, maxlength: 12 },
    createdAt: { type: Date, default: Date.now },
  }, {
    versionKey: false
  }
);

UserSchema.plugin(uniqueValidator);

// Validate email
UserSchema.path('email').validate(email => {
    let emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailRegex.test(email);
}, 'Email is wrong');

// Set the createdAt parameter equal to the current time
UserSchema.pre('save', function(next) {
  let now = new Date();
  if(!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

module.exports = mongoose.model('user', UserSchema);