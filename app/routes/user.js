let User = require('../models/user');

module.exports = {

  /*
   * Get all users
   */
  getUsers(req, res) {
    //Query the DB and if no errors, send all the books
    let query = User.find({});
    query.exec((err, users) => {
      if (err) res.send(err);
      //If no errors, send them back to the client
      res.json(users);
    });
  },

  /*
   * Post new user
   */
  postUser(req, res) {
    //Creates a new user
    let newUser = new User(req.body);
    //Save it into the DB.
    newUser.save((err, user) => {
      if (err) {
        res.send(err);
      }
      else { //If no errors, send it back to the client
        res.json({message: "User successfully added!", user});
      }
    });
  },

  /*
   * GET /user/:email route to retrieve a user given its id.
   */
  getUser(req, res) {
    User.findOne({email: req.params.email}, (err, user) => {
      if (err) res.send(err);
      //If no errors, send it back to the client
      res.json(user);
    });
  },

	/*
	 * DELETE /user/:email to delete a user given its email.
	 */
  deleteUser(req, res) {
    User.remove({email: req.params.email}, (err, result) => {
      res.json({message: "User successfully deleted!", result});
    });
  },

	/*
	 * PUT /user/:email to update a user given its email
	 */
  updateUser(req, res) {
    User.findOne({email: req.params.email}, (err, user) => {
      if (err) {
        res.send(err);
        return;
      }
      Object.assign(user, req.body).save((err, user) => {
        if (err) {
          res.send(err);
          return;
        }
        res.json({message: 'User updated!', user});
      });
    });
  }
};
