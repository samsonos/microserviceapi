const User = require('../models/user');
const nodemailer = require('nodemailer');
const config = require('config');

let security = {
  handleResponse(err, user, res, response) {
    if (err) {
      res.send(err);
    } else if (!user) {
      res.status(404);
      res.send({
        error: 'User not found',
        status: 0
      });
    } else {
      res.json(Object.assign({status: 1, user}, response));
    }
  },

  /*
   * Forgot password
   */
  forgot(req, res) {
    User.findOne({email: req.body.email}, (err, user) => {
      if (err) {
        res.send(err);
        return;
      }
      if (!user) {
        res.status(404);
        res.send({
          error: 'User not found'
        });
        return;
      }

      // Create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: config.get('gmailUserName'),
          pass: config.get('gmailPassword')
        }
      });

      // Setup email data with unicode symbols
      let mailOptions = {
        from: 'info@samsonos.com', // sender address
        to: user.email, // list of receivers
        subject: 'Password reminder', // Subject line
        text: `Your password is: ${user.password}`, // plain text body
      };

      // Send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        res.json({message: 'Email was sent', status: 1});
      });
    });
  },

  /*
   * Login user
   */
  login(req, res) {
    User.findOne({email: req.body.email, password: req.body.password}, (err, user) => {
      security.handleResponse(err, user, res, {});
    });
  }
};

module.exports = security;
